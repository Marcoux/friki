﻿using Supervisors;
using UnityEngine;

namespace Monsters.Scripts.Animate
{
    internal class InteractBehaviour : StateMachineBehaviour
    {
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            GameObject.FindGameObjectWithTag(TagManager.MONSTER)
                .GetComponent<Monster>()
                .UpdateState(stateInfo);
        }
    }
}
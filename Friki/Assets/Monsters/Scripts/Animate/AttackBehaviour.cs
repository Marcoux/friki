﻿using Supervisors;
using UnityEngine;

namespace Monsters.Scripts.Animate
{
    internal class AttackBehaviour : StateMachineBehaviour
    {
        private Monster monster;

        private void Awake()
        {
            monster = GameObject.FindGameObjectWithTag(TagManager.MONSTER)
                .GetComponent<Monster>();
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            monster.attackCollider.enabled = true;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            monster.attackCollider.enabled = false;
        }
    }
}

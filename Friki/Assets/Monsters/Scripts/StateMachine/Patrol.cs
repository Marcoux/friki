using System.Collections.Generic;
using Monsters.Scripts.Attributes;
using Supervisors;
using UnityEngine;

namespace Monsters.Scripts.StateMachine
{
    /// <summary>
    /// Class encapsulating how a Monster patrol in a level.
    /// </summary>
    internal sealed class Patrol : State
    {
        private readonly Queue<Vector3> destinations;

        public Patrol(int capacity = 5)
        {
            destinations = new Queue<Vector3>(capacity);
            foreach (var position in Director.RandomRoomsWaypoint(capacity))
            {
                destinations.Enqueue(position);
            }
        }

        public State Transition(Movement movement, Hearing hearing, Sight sight)
        {
            if (movement.wantsToTraverseDoorFrame is true)
            {
                return new OpenDoor(this);
            }

            var playerPosition = Director.PlayerPosition;

            if (sight.InFieldOfView(playerPosition) is true)
            {
                return new Chase();
            }

            if (hearing.InFieldOfHearing(Director.PlayerMovement, playerPosition) is true)
            {
                movement.WalkToPosition(Director.PlayerNearestWaypoint());
                return this;
            }

            if (movement.pathPending is false && movement.nearDestination is true)
            {
                if (destinations.Count == 0)
                {
                    // reached the limit of waypoint without finding the player, will now select a waypoint near player
                    movement.WalkToPosition(Director.PlayerNearestWaypoint());
                    return new Patrol(0);
                }

                movement.WalkToPosition(destinations.Dequeue());
            }

            return this;
        }
    }
}
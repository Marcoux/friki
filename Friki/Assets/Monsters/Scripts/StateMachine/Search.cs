using System.Collections.Generic;
using Monsters.Scripts.Attributes;
using Supervisors;
using UnityEngine;

namespace Monsters.Scripts.StateMachine
{
    /// <summary>
    /// Class encapsulating how a Monster search a player.
    /// </summary>
    internal sealed class Search : State
    {
        private readonly Queue<Vector3> destinations;

        public Search(int capacity = 3)
        {
            destinations = new Queue<Vector3>(capacity);
            foreach (var position in Director.PlayerNearestWaypoints(capacity))
            {
                destinations.Enqueue(position);
            }
        }

        public State Transition(Movement movement, Hearing hearing, Sight sight)
        {
            if (movement.wantsToTraverseDoorFrame is true)
            {
                return new OpenDoor(this);
            }

            var playerPosition = Director.PlayerPosition;

            if (sight.InFieldOfView(playerPosition) is true)
            {
                return new Chase();
            }

            if (hearing.InFieldOfHearing(Director.PlayerMovement, playerPosition) is true)
            {
                movement.WalkToPosition(Director.PlayerNearestWaypoint());
                return this;
            }

            if (movement.isIdle is true || (movement.pathPending is false && movement.nearDestination is true))
            {
                if (destinations.Count == 0)
                {
                    return new Patrol();
                }

                movement.WalkToPosition(destinations.Dequeue());
            }

            return this;
        }
    }
}
using Monsters.Scripts.Attributes;
using Supervisors;

namespace Monsters.Scripts.StateMachine
{
    /// <summary>
    /// Class encapsulating how a Monster chase a player.
    /// </summary>
    internal sealed class Chase : State
    {
        public State Transition(Movement movement, Hearing hearing, Sight sight)
        {
            if (movement.wantsToTraverseDoorFrame is true)
            {
                return new OpenDoor(this);
            }

            var playerPosition = Director.PlayerPosition;

            if (sight.InFieldOfView(playerPosition) is false)
            {
                return new Search();
            }

            if (sight.InAttackRange(playerPosition) is true)
            {
                return new Attack();
            }

            movement.RunToPosition(playerPosition);
            return this;
        }
    }
}
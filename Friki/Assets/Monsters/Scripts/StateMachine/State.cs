using Monsters.Scripts.Attributes;

namespace Monsters.Scripts.StateMachine
{
    internal interface State
    {
        /// <summary>
        /// Allow monster to go from a state to another
        /// </summary>
        /// <param name="movement"></param>
        /// <param name="hearing"></param>
        /// <param name="sight"></param>
        /// <returns></returns>
        State Transition(Movement movement, Hearing hearing, Sight sight);
    }
}
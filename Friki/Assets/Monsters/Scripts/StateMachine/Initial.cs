using Monsters.Scripts.Attributes;

namespace Monsters.Scripts.StateMachine
{
    /// <summary>
    /// Class encapsulating the initial state of a Monster.
    /// </summary>
    internal sealed class Initial : State
    {
        public State Transition(Movement movement, Hearing hearing, Sight sight)
        {
            return new Patrol();
        }
    }
}
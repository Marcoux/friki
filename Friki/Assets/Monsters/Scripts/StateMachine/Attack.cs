using Monsters.Scripts.Attributes;
using Supervisors;

namespace Monsters.Scripts.StateMachine
{
    /// <summary>
    /// Class encapsulating how a Monster attack the player.
    /// </summary>
    internal sealed class Attack : State
    {
        public State Transition(Movement movement, Hearing hearing, Sight sight)
        {
            // because monster need to be Idle before it can attack (or go back to walk/run)
            if (movement.isIdle is false)
            {
                movement.Idle();
                return this;
            }

            if (Director.PlayerPosition is var p && sight.InAttackRange(p) is false)
            {
                return new Chase();
            }

            movement.Attack();
            return this;
        }
    }
}
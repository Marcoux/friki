﻿using Monsters.Scripts.Attributes;

namespace Monsters.Scripts.StateMachine
{
    internal sealed class OpenDoor : State
    {
        private readonly State state;

        public OpenDoor(State state)
        {
            this.state = state;
        }

        public State Transition(Movement movement, Hearing hearing, Sight sight)
        {
            if (movement.IsCurrentlyTraverseDoorFrame is false)
            {
                movement.PrepareToTraverseDoorFrame();
            }

            movement.SetOrientationToDoorFrame();

            if (movement.WantsOpenDoor is false)
            {
                if (movement.HasDoorFront())
                {
                    movement.Idle();
                    return this;
                }
            }
            else if (movement.HasInteractWithDoor is false)
            {
                //Animate interact with door
                movement.AnimateInteractWithDoor();
                return this;
            }
            else if (movement.HasInteractWithDoor is true && movement.IsCurrentlyOpeningDoor is false)
            {
                movement.OpenDoor();
                return this;
            }
            else if (movement.HasFinishOpenDoor is false)
            {
                //Wait the door animation
                return this;
            }

            if (movement.GoThroughDoorFrame() is false)
            {
                //Not finish passing doorFrame, monster continue
                return this;
            }
            else
            {
                //Monster return to previous state before OpenDoor state
                return state;
            }
        }
    }
}
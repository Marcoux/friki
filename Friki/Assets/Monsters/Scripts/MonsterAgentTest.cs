﻿using System.Collections;
using Monsters.Scripts.Attributes;
using UnityEngine;
using UnityEngine.AI;

namespace Monsters.Scripts
{
    public class MonsterAgentTest : MonoBehaviour
    {
        [SerializeField]
        public Transform[] points;
        public float waitingTime = 3.5f;
        public bool targetPlayer = false;
        public float speedRunning = 7.0f;
        public float speedwalk = 1.0f;
        public Transform playerTransform;

        private int destPoint = 0;
        private NavMeshAgent agent;
        private bool needToWait = false;
        private bool processingWait = false;

        private Movement movements;
        private Sight sight;

        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            movements = new Movement(agent, GetComponent<Animator>(), speedwalk, speedRunning);
            sight = new Sight(transform);
        }

        public void OnDrawGizmos()
        {
            if(sight != null)
                sight.DrawGizmos();
        }

        // Update is called once per frame
        void Update()
        {
            if (targetPlayer)
            {
                movements.RunToPosition(playerTransform.position);
                if (!agent.pathPending && agent.remainingDistance <= 2f)
                {
                    movements.Idle();
                }
                return;
            }

            if (!agent.pathPending && agent.remainingDistance <= 0.1f)
            {
                if (needToWait)
                {
                    if (!processingWait)
                    {
                        processingWait = true;
                        StartCoroutine(WaitAndCheckNextPoint());
                    }
                }
                else
                {
                    needToWait = true;
                    GoToRandomPoint();
                }
            }
        }

        private IEnumerator WaitAndCheckNextPoint()
        {
            movements.Idle();
            yield return new WaitForSeconds(waitingTime);
            GoToRandomPoint();
            agent.isStopped = false;
            processingWait = false;
        }

        private void SetDestination(Vector3 position)
        {
            movements.WalkToPosition(position);
        }

        private void GoToRandomPoint()
        {
            if (points.Length == 0)
                return;

            int newDestPoint = destPoint;

            while (newDestPoint == destPoint)
            {
                newDestPoint = Random.Range(0, points.Length);
            }

            destPoint = newDestPoint;
            SetDestination(points[destPoint].position);
        }
    }
}

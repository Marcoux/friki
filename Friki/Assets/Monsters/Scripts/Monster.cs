﻿using Monsters.Scripts.Attributes;
using Monsters.Scripts.StateMachine;
using Supervisors;
using UnityEngine;
using UnityEngine.AI;

namespace Monsters.Scripts
{
    public class Monster : MonoBehaviour
    {
        private Hearing hearing;
        private Sight sight;
        private State currentState;
        internal BoxCollider attackCollider;
        
        internal Movement movement;

        internal void UpdateState(AnimatorStateInfo stateInfo) => movement?.OnAnimationExit(stateInfo); 

        private void Awake()
        {
            movement = new Movement(GetComponent<NavMeshAgent>(), GetComponent<Animator>());
            hearing = new Hearing(transform);
            sight = new Sight(transform);
            attackCollider = GetComponent<BoxCollider>();
        }

        private void Start()
        {
            currentState = new Initial();
        }

        private void Update()
        {
            currentState = currentState.Transition(movement, hearing, sight);
        }

        private void OnDrawGizmos()
        {
            hearing?.DrawGizmos();
            sight?.DrawGizmos();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (currentState is Attack && other.transform.CompareTag(TagManager.PLAYER))
            {
                SceneLoader.LoadGameOver();
            }
        }
    }
}
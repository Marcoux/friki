﻿using System;
using LevelDesign.Scripts;
using Supervisors;
using UnityEngine;
using UnityEngine.AI;

namespace Monsters.Scripts.Attributes
{
    internal sealed class Movement
    {
        private const float DEFAULT_WALK_SPEED = 6.0f;
        private const float DEFAULT_RUNNING_SPEED = 10.0f;

        private const float ROTATION_SPEED = 0.1f;
        private const float MAX_RADIUS_DOOR_DETECTION = 1.0f;

        private static readonly int movingHashTag = Animator.StringToHash("moving");
        private static readonly int battleHashTag = Animator.StringToHash("battle");
        private static readonly System.Random rng = new System.Random();

        private readonly NavMeshAgent agent;
        private readonly Animator animator;
        private readonly float runningSpeed;
        private readonly float walkSpeed;

        private Vector3 endPositionMeshLink;
        private RaycastHit doorHit;

        public event Action<ActionValue> OnActionChange;
        public event Action<BattleValue> OnBattleChange;

        //Enum for actions of monster (Different animation)
        public enum ActionValue
        {
            Idle = 0,
            WalkAndRunning = 1,
            Attack1 = 2,
            Attack2 = 3,
            Attack3 = 4,
            OpenDoor = 16
        }

        //Enum to know if monster is in battle state
        public enum BattleValue
        {
            NotInBattle = 0,
            InBattle = 1
        }

        public Movement(
            NavMeshAgent agent,
            Animator animator,
            float walkSpeed = DEFAULT_WALK_SPEED,
            float runningSpeed = DEFAULT_RUNNING_SPEED)
        {
            this.agent = agent;
            this.animator = animator;

            this.walkSpeed = walkSpeed;
            this.runningSpeed = runningSpeed;

            // Deactivate auto traverse to make our own traverse function.
            this.agent.autoTraverseOffMeshLink = false;
        }

        /// <summary>
        /// True if agent wants to enter in a room or exit a room
        /// </summary>
        public bool wantsToTraverseDoorFrame =>
            agent.isOnOffMeshLink;

        /// <summary>
        /// True if the navmesh agent is stopped (which mean that the idle animation is playing). False otherwise.
        /// </summary>
        public bool isIdle =>
            agent.isStopped;

        /// <summary>
        /// True if agent is at or near destination. False otherwise.
        /// </summary>
        public bool nearDestination =>
            agent.remainingDistance <= agent.stoppingDistance && agent.velocity.sqrMagnitude == 0f;

        /// <summary>
        /// True if agent has a destination but need time to compute path to it. False otherwise.
        /// </summary>
        public bool pathPending =>
            agent.pathPending;

        public bool IsCurrentlyTraverseDoorFrame { get; private set; }

        public bool IsCurrentlyOpeningDoor { get; private set; }

        public bool WantsOpenDoor { get; private set; }

        public bool HasInteractWithDoor { get; private set; }

        public bool HasFinishOpenDoor { get; private set; }

        /// <summary>
        /// If the current animation is "idle", then this method will transition to one of the "attack" animation
        /// </summary>
        /// <remarks>
        /// You need to go back to the "idle" animation before transitioning to "walk" or "run"!
        /// </remarks>
        public void Attack()
        {
            // 2, 3 and 4 correspond to the monster's attack animation
            var randomAttack = rng.Next(2, 5);

            animator.SetInteger(battleHashTag, (int) BattleValue.InBattle);
            OnBattleChange?.Invoke(BattleValue.InBattle);

            animator.SetInteger(movingHashTag, randomAttack);
            OnActionChange?.Invoke((ActionValue) randomAttack);
        }

        public void Idle()
        {
            animator.SetInteger(movingHashTag, (int) ActionValue.Idle);
            OnActionChange?.Invoke(ActionValue.Idle);
            agent.isStopped = true;
        }

        public void WalkToPosition(Vector3 position)
        {
            animator.SetInteger(battleHashTag, (int) BattleValue.NotInBattle);
            OnBattleChange?.Invoke(BattleValue.NotInBattle);

            agent.speed = walkSpeed;
            AnimateAndMoveToPosition(position);
        }

        public void RunToPosition(Vector3 position)
        {
            animator.SetInteger(battleHashTag, (int) BattleValue.InBattle);
            OnBattleChange?.Invoke(BattleValue.InBattle);

            agent.speed = runningSpeed;
            AnimateAndMoveToPosition(position);
        }

        public void PrepareToTraverseDoorFrame()
        {
            IsCurrentlyTraverseDoorFrame = true;
            OffMeshLinkData dataMeshLink = agent.currentOffMeshLinkData;
            endPositionMeshLink = dataMeshLink.endPos;
            //Keep y position to agent position
            endPositionMeshLink.y = agent.transform.position.y;
        }

        public bool GoThroughDoorFrame()
        {
            bool finishPassingDoorFrame = false;

            //We do walking or run animation after door open
            if (HasFinishOpenDoor)
                animator.SetInteger(movingHashTag, (int) ActionValue.WalkAndRunning);

            if (Vector3.Distance(agent.transform.position,endPositionMeshLink) > 0.001f)
            {
                //Move agent manually to destination
                agent.transform.position = Vector3.MoveTowards(agent.transform.position, endPositionMeshLink,
                    agent.speed * Time.deltaTime);
            }
            else
            {
                //Signal that agent is on the other side of the mesh link
                agent.CompleteOffMeshLink();
                agent.isStopped = false;
                IsCurrentlyTraverseDoorFrame = false;
                CloseDoor();
                finishPassingDoorFrame = true;
            }

            return finishPassingDoorFrame;
        }

        public void OpenDoor()
        {
            Idle();
            IsCurrentlyOpeningDoor = true;
            InteractWithDoor();
        }

        public void AnimateInteractWithDoor()
        {
            animator.SetInteger(movingHashTag, (int) ActionValue.OpenDoor);
        }

        public bool HasDoorFront()
        {
            if (Physics.Raycast(agent.transform.position, agent.transform.forward, out doorHit,
                MAX_RADIUS_DOOR_DETECTION))
            {
                bool hasDoorFront = doorHit.transform.CompareTag(TagManager.DOOR_CLOSE);
                WantsOpenDoor = hasDoorFront;
                return hasDoorFront;
            }

            return false;
        }

        public void SetOrientationToDoorFrame()
        {
            Vector3 lookPos = endPositionMeshLink - agent.transform.position;
            lookPos.y = 0;
            if (lookPos != Vector3.zero)
            {
                Quaternion rotation = Quaternion.LookRotation(lookPos);
                agent.transform.rotation = Quaternion.Slerp(agent.transform.rotation, rotation, ROTATION_SPEED);
            }
        }

        internal void OnAnimationExit(AnimatorStateInfo stateInfo)
        {
            if (stateInfo.IsTag(TagManager.DOOR_OPENING))
            {
                HasFinishOpenDoor = true;
            }
            else if (stateInfo.IsTag(TagManager.DOOR_OPEN))
            {
                HasInteractWithDoor = true;
            }
        }

        private void CloseDoor()
        {
            InteractWithDoor();
            WantsOpenDoor = false;
            IsCurrentlyOpeningDoor = false;
            HasFinishOpenDoor = false;
            HasInteractWithDoor = false;
        }

        private void InteractWithDoor()
        {
            var doorTransform = doorHit.transform;
            if (doorTransform == null)
                return;

            var doorObject = doorTransform.gameObject;
            if (doorObject == null)
                return;

            //Fix OpenDoor - Null reference when doorObject doesn't have a DoorController
            DoorController doorController = doorObject.GetComponent<DoorController>();
            if (doorController == null)
                return;

            doorController.Interact();
        }

        /// <summary>
        /// Called by walk and run to animate monster
        /// </summary>
        /// <param name="position">the place to go to</param>
        private void AnimateAndMoveToPosition(Vector3 position)
        {
            animator.SetInteger(movingHashTag, (int) ActionValue.WalkAndRunning);
            OnActionChange?.Invoke(ActionValue.WalkAndRunning);

            agent.isStopped = false;
            agent.SetDestination(position);
        }
    }
}
﻿using Supervisors;
using UnityEngine;

namespace Monsters.Scripts.Attributes
{
    internal sealed class Sight
    {
        private const float DEFAULT_ATTACK_RANGE = 3.0f;
        private const float DEFAULT_MAX_ANGLE = 60.0f;
        private const float DEFAULT_MAX_RADIUS = 15.0f;
        private const float RANGE_Y_MIN = 1.0f;
        private const float RANGE_Y_MAX = 2.0f;

        private readonly float attackRange;
        private readonly float sightAngle;
        private readonly float sightRange;
        private readonly Transform monsterTransform;

        public Sight(
            Transform monsterTransform,
            float attackRange = DEFAULT_ATTACK_RANGE,
            float sightAngle = DEFAULT_MAX_ANGLE,
            float sightRange = DEFAULT_MAX_RADIUS)
        {
            this.attackRange = attackRange;
            this.sightAngle = sightAngle;
            this.sightRange = sightRange;

            this.monsterTransform = monsterTransform;
        }

        public void DrawGizmos()
        {
            var monsterPosition = monsterTransform.position;
            var monsterForward = monsterTransform.forward;
            var monsterUp = monsterTransform.up;

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(monsterPosition, sightRange);

            var fovLine1 = Quaternion.AngleAxis(sightAngle, monsterUp) * monsterForward * sightRange;
            var fovLine2 = Quaternion.AngleAxis(-sightAngle, monsterUp) * monsterForward * sightRange;

            Gizmos.color = Color.blue;
            Gizmos.DrawRay(monsterPosition, fovLine1);
            Gizmos.DrawRay(monsterPosition, fovLine2);

            Gizmos.color = Color.black;
            Gizmos.DrawRay(monsterPosition, monsterForward * sightRange);
        }

        public bool InFieldOfView(Vector3 target) => InRange(target, sightRange);

        public bool InAttackRange(Vector3 target) => InRange(target, attackRange);

        private bool InRange(Vector3 other, float range)
        {
            bool inRange = false;
            for (float rangeY = RANGE_Y_MIN; rangeY <= RANGE_Y_MAX && inRange is false; rangeY+=0.5f)
            {
                var origin = SetYPlane(monsterTransform.position, rangeY);
                var target = SetYPlane(other, rangeY);
                var direction = (target - origin).normalized;

                var angle = Vector3.Angle(direction, monsterTransform.forward);
                if (angle <= sightAngle)
                {
                    // target in sight! But is it in range?
                    if (Physics.Raycast(origin, direction, out var hitTarget, range))
                    {
                        // target in sight and in range! But is it the player?
                        inRange = hitTarget.transform.CompareTag(TagManager.PLAYER);
                    }
                }
            }

            return inRange;
        }
        
        private static Vector3 SetYPlane(Vector3 vector, float y)
        {
            vector.y = y;
            return vector;
        }
    }
}
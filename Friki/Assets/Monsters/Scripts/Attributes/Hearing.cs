using System;
using Player.Scripts;
using UnityEngine;

namespace Monsters.Scripts.Attributes
{
    internal sealed class Hearing
    {
        private static readonly System.Random rng = new System.Random();
        
        private readonly float hearingRange;
        private readonly Transform transform;

        public Hearing(Transform transform)
        {
            this.transform = transform;
            this.hearingRange = 25.0f;
        }

        public void DrawGizmos()
        {
            var position = transform.position;
            var forward = transform.forward;

            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(position, hearingRange);

            Gizmos.color = Color.black;
            Gizmos.DrawRay(position, forward * hearingRange);
        }

        public bool InFieldOfHearing(PlayerMovement.State movement, Vector3 position)
        {
            switch (movement)
            {
                case PlayerMovement.State.Hidden:
                case PlayerMovement.State.Idle:
                    return false;
                case PlayerMovement.State.Crouching:
                    return InFieldOfHearing(position, 10f) && TryToHear(0.001f);
                case PlayerMovement.State.Walking:
                    return InFieldOfHearing(position, 20.0f) && TryToHear(0.05f);
                case PlayerMovement.State.Running:
                    return InFieldOfHearing(position, 50.0f) && TryToHear(0.1f);
                default:
                    throw new ArgumentOutOfRangeException(nameof(movement), movement, null);
            }
        }

        private bool InFieldOfHearing(Vector3 other, float range)
        {
            var origin = SetYPlane(transform.position, 0);
            var target = SetYPlane(other, 0);

            return (target - origin).magnitude <= range;
        }

        private static bool TryToHear(float chance) =>
            rng.NextDouble() < chance;

        private static Vector3 SetYPlane(Vector3 vector, float y)
        {
            vector.y = y;
            return vector;
        }
    }
}
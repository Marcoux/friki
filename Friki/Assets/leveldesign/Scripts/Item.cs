﻿using Player.Scripts;
using UnityEngine;

namespace leveldesign.Scripts
{
    public class Item : MonoBehaviour, IInteractable
    {
        [SerializeField] private AudioClip clip;
        public string title;

        private Sprite image; 

        public void Start()
        {
            image = Resources.Load<Sprite>("Sprite/" + title);
        }

        public Sprite GetImage()
        {
            return image;
        }

        public void Interact(GameObject player)
        {
            var inventory = player.GetComponent<PlayerInventory>();
            inventory.StoreItem(this);
            
            AudioSource.PlayClipAtPoint(clip, transform.position);
            Destroy(gameObject);
        }
    }
}
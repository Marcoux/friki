﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace leveldesign.Scripts
{
    public class ItemSpawn : MonoBehaviour
    {
        [Range(1, 10)] [SerializeField] private int numberOfKey;
        [SerializeField] private GameObject[] keys;

        private static readonly Random rng = new Random();

        public int NumberOfKey => numberOfKey;

        private void Start()
        {
            var tuples = GetComponentsInChildren<Transform>()
                .OrderBy(spawn => rng.Next())
                .Take(numberOfKey)
                .Zip(RandomKeys(numberOfKey), (spawn, key) => (spawn, key));

            foreach (var (spawn, key) in tuples)
            {
                Instantiate(key, spawn);
            }
        }

        private IEnumerable<GameObject> RandomKeys(int size) =>
            Enumerable.Range(0, size).Select(_ => RandomKey());

        private GameObject RandomKey()
        {
            var index = rng.Next(0, keys.Length);
            return keys[index];
        }
    }
}
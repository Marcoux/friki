﻿using UnityEngine;
using Player.Scripts;
using Random = System.Random;

namespace LevelDesign.Scripts
{
    public class DoorController : MonoBehaviour, IInteractable
    {
        [SerializeField] private AudioClip[] openingClips;
        [SerializeField] private AudioClip[] creakingClips;

        private static readonly Random rng = new Random();
        private static readonly int IsOpening = Animator.StringToHash("IsOpening");

        private Animator doorAnimator;
        private AudioSource audioSource;

        // Start is called before the first frame update
        private void Start()
        {
            doorAnimator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
        }

        private void PlayRandomOpeningClip()
        {
            var index = rng.Next(0, openingClips.Length);
            audioSource.PlayOneShot(openingClips[index]);
        }

        private void PlayRandomCreakingClip()
        {
            var index = rng.Next(0, creakingClips.Length);

            audioSource.clip = creakingClips[index];
            audioSource.PlayDelayed(1.0f);
        }

        public void Interact()
        {
            doorAnimator.SetBool(IsOpening, !doorAnimator.GetBool(IsOpening));

            if (audioSource.isPlaying is false)
            {
                PlayRandomOpeningClip();
                PlayRandomCreakingClip();
            }
        }
        
        public void Interact(GameObject player) =>
            Interact();
    }
}
﻿using UnityEngine;
using Player.Scripts;

namespace LevelDesign.Scripts
{
    public class LightSwitchesController : MonoBehaviour, IInteractable
    {
        [SerializeField] private GameObject[] allLights;
        private Renderer _rend;
        private bool _switching;
        private static readonly int ColorProp = Shader.PropertyToID("_Color");

        private void Start()
        {
            _rend = gameObject.transform.GetChild(0).GetComponent<Renderer>();
            _rend.material.SetColor(ColorProp, Color.yellow);
            _switching = true;
        }

        public void Interact(GameObject player)
        {
            _rend.material.SetColor(ColorProp, _switching ? Color.blue : Color.yellow);
            foreach (var i in allLights)
            {
                i.SetActive(!i.activeSelf);
            }
            _switching = !_switching;
        }
    }
}

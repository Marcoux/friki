using LevelDesign.Scripts.Switch;
using Player.Scripts;
using UnityEngine;

namespace LevelDesign.Scripts
{
    public class UnlockableDoor : DoorController, IInteractable
    {
        [SerializeField] private GameObject unlocker;

        private bool Open { get; set; } = false;

        private void Awake()
        {
            gameObject.name = "Locked Door";
            unlocker.GetComponent<DoorUnlocker>().OnInteract += open =>
            {
                Open = open;
                gameObject.name = "Door";
            };
        }

        public new void Interact(GameObject player)
        {
            if (Open is true)
            {
                base.Interact();
            }
        }
    }
}
﻿using Player.Scripts;
using Supervisors;
using UnityEngine;

namespace leveldesign.Scripts
{
    public class SuccessDoor : MonoBehaviour, IInteractable
    {
        [SerializeField] private GameObject spawner;

        private int requiredKeys;

        private void Awake()
        {
            requiredKeys = spawner.GetComponent<ItemSpawn>().NumberOfKey;
            gameObject.name = $"Locked Door - I need {requiredKeys} key(s)";
        }

        public void Interact(GameObject player)
        {
            var playerKeys = player
                .GetComponent<PlayerInventory>()
                .size;

            if (playerKeys >= requiredKeys)
            {
                SceneLoader.LoadVictory();
            }
            else
            {
                gameObject.name = $"Locked Door - {requiredKeys - playerKeys} key(s) left";
            }
        }
    }
}
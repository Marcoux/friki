using System;
using Player.Scripts;
using UnityEngine;

namespace LevelDesign.Scripts.Switch
{
    public class DoorUnlocker : MonoBehaviour, IInteractable
    {
        public event Action<bool> OnInteract;
        private Renderer _rend;
        private static readonly int ColorProp = Shader.PropertyToID("_Color");
        
        private void Start()
        {
            _rend = gameObject.transform.GetChild(0).GetComponent<Renderer>();
            _rend.material.SetColor(ColorProp, Color.red);
        }
        private void Awake() =>
            gameObject.name = "Unlock Door";

        public void Interact(GameObject player)
        {
            _rend.material.SetColor(ColorProp, Color.green);
            OnInteract?.Invoke(true);
            gameObject.name = "Door Unlocked";
        }
    }
}
﻿using UnityEngine;

namespace LevelDesign.Scripts
{
    public class LightsOff : MonoBehaviour
    {
        private void Start()
        {
            foreach (var i in GameObject.FindGameObjectsWithTag("OnStartOff"))
            {
                i.SetActive(false);
            }
        }
    }
}

﻿using Player.Scripts;
using UnityEngine;

namespace LevelDesign.Scripts
{
    public class NotesControllerScript : MonoBehaviour, IInteractable
    {
        bool reading = false;

        //The maximum range needs to increment each time we add a new text to the TextManager
        [Range(0, 9)]
        public int textValue = 0;

        private void Update()
        {
            if (reading)
            {
                if (Input.GetButtonDown(InputManager.INTERACT_INPUT))
                {
                    NoteDisplay.instance.gameObject.SetActive(false);
                    reading = false;
                }
            }
        }

        public void Interact(GameObject player)
        {
            NoteDisplay.instance.SetNoteText(TextManager.listNotes[textValue]);
            NoteDisplay.instance.ToggleActive();
            reading = true;
        }
    }
}

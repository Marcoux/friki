﻿using UnityEngine;
using UnityEngine.UI;

namespace LevelDesign.Scripts
{
    public class NoteDisplay : MonoBehaviour
    {
        public static NoteDisplay instance;

        private void Awake()
        {
            instance = this;
        }

        void Start()
        {
            transform.gameObject.SetActive(false);
            transform.GetChild(0).GetComponent<Text>().text = null;

        }

        public void SetNoteText(string text)
        {
            transform.GetChild(0).GetComponent<Text>().text = text;
        }

        public void ToggleActive()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }
    }
}

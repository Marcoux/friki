﻿using System.Collections.Generic;

namespace LevelDesign.Scripts
{
    public static class TextManager
    {
        public static readonly List<string> listNotes = new List<string> {
            "<b>Objectif: Ramasser 3 sceaux en métal, pour ouvrir la porte finale.</b>\n \nPour vous aider, des maps sur les murs sont à votre disposition un peu partout.\nAinsi, d'autres \"clipboards\" comme celui-ci, pour vous guider.\n \nPS: L'énergie de votre lampe de poche n'est pas récupérable, donc faites-en bon usage.",
            "<b>Porte barrée?</b> Il faut accéder à un interrupteur pour débarrer une porte. Cependant, il y en a un peu partout pour différentes portes.",
            "<b>Vous pouvez intéragir avec les interrupteurs qui sont dispersés un peu partout.</b>\n \nPS: Il y a 2 types d'interrupteurs:\n - Allumer/Éteindre des lumières.\n - Débarrer des portes qui sont barrées.",
            "<b>Vous pouvez intéragir avec ces interrupteurs. Quand vous allez intéragir avec un interrupteur, cela va faire en sorte d'interchanger l'état (Allumer ou Éteindre) de plusieurs lumières à la fois.</b>" +
            "\nAinsi, cela va vous permettre de trouver une bonne combinaison pour arriver à un bon résultat.\n \nPS: Il y a 2 types d'interrupteurs:\n - Allumer/Éteindre des lumières.\n - Débarrer des portes qui sont barrées."
        };
    }
}

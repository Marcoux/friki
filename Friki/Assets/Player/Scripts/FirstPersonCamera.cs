﻿using UnityEngine;

namespace Player.Scripts
{
    public class FirstPersonCamera : MonoBehaviour
    {
        public Transform playerBody;
        public float mouseSensitivity = 100f;   //Not sure if needed, can't seem to spot the difference across multiple values
        private float xRotation = 0f;
        private float yRotation = 0f;
        public float lookupMaxAngle = -90f;
        public float lookDownMaxAngle = 55f;

        public float lookLeftMaxAngle = -75f;
        public float lookRightMaxAngle = 75f;
        
        private const float CAMERA_YZ_EULER = 0f;

        void Start()
        {
            //Cursor always in the center of the view
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        void Update()
        {
            float mouseX = Input.GetAxis(InputManager.HORIZONTAL_AXIS) * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis(InputManager.VERTICAL_AXIS) * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, lookupMaxAngle, lookDownMaxAngle);

            yRotation += mouseX;
            yRotation = Mathf.Clamp(yRotation, lookLeftMaxAngle, lookRightMaxAngle);

            transform.localRotation = Quaternion.Euler(xRotation, CAMERA_YZ_EULER, CAMERA_YZ_EULER);

            if (playerBody.gameObject.GetComponent<PlayerMovement>().GetHiddenStatus())
            {
                transform.localRotation = Quaternion.Euler(xRotation, yRotation, CAMERA_YZ_EULER);
            }
            else
            {
                playerBody.Rotate(Vector3.up * mouseX);
            }
        }
    }
}
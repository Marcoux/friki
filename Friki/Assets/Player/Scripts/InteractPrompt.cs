﻿using UnityEngine;
using UnityEngine.UI;

namespace Player.Scripts
{
    public class InteractPrompt : MonoBehaviour
    {
        public GameObject selectedObjectName;
        public GameObject selectedObjectAction;    //Unused for now since we just put the same message for every interactable

        public static InteractPrompt instance;
        // Start is called before the first frame update

        private void Awake()
        {
            instance = this;
        }

        void Start()
        {
            selectedObjectName.GetComponent<Text>().text = "";
        }

        public void SetObjectName(string objectName)
        {
            selectedObjectName.GetComponent<Text>().text = objectName;
        }

        public void SetObjectAction(string actionName)
        {
            selectedObjectAction.GetComponent<Text>().text = actionName;
        }
    }
}


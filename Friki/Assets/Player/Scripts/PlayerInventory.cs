﻿using leveldesign.Scripts;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Player.Scripts
{
    public class PlayerInventory : MonoBehaviour
    {
        private List<Item> items;
        private List<GameObject> images;

        public Canvas canvas;

        private void Awake()
        {
            items = new List<Item>();
            images = new List<GameObject>();
        }

        public int size =>
            items.Count;
        public void StoreItem(Item item)
        {
            items.Add(item);
            addToHeadsUp(items[items.Count - 1]);
        }

        private void addToHeadsUp(Item item)
        {
            GameObject image = new GameObject("Image");
            image.AddComponent<RectTransform>();
            image.AddComponent<CanvasRenderer>();
            image.AddComponent<Image>();
            image.GetComponent<Image>().sprite = item.GetImage();

            RectTransform r = image.GetComponent<RectTransform>();
            r.sizeDelta = new Vector2(50, 50);

            if (images.Count == 0)
                r.anchoredPosition = new Vector3(10, 0, 0);
            else
            {
                RectTransform i = images[images.Count - 1].GetComponent<RectTransform>();
                r.anchoredPosition = new Vector2( i.anchoredPosition.x + 55, 0);
            }

            GameObject instance = Instantiate(image);
            images.Add(instance);
            instance.transform.SetParent(canvas.transform, false);
        }
    }
}

﻿using UnityEngine;

namespace Player.Scripts
{
    public class FlashlightToggleScript : MonoBehaviour
    {
        
        private Light light;
        private FlashlightBar batteryBar;
        public float batteryDepletionRate = 0.05f;

        private void Start()
        {
            light = gameObject.GetComponent<Light>();
            batteryBar = FlashlightBar.instance;
        }

        private void Update()
        {
            if (Time.timeScale == 0)
            {
                return;
            }

            ToggleFlashlight();

            if (light.enabled)
            {
                batteryBar.UseBattery(batteryDepletionRate);

                if (batteryBar.GetCurrentBatteryLevel() <= 0)
                {
                    light.enabled = false;
                }
            }
        }

        private void ToggleFlashlight()
        {
            if (Input.GetButtonDown(InputManager.FLASHLIGHT_INPUT) && batteryBar.GetCurrentBatteryLevel() > 0)
            {
                light.enabled = !light.enabled;
            }
        }
    }
}


﻿using Player.Scripts;
using UnityEngine;

namespace Player.Scripts
{
    public class HidingPlaceScript : MonoBehaviour, IInteractable
    {
        public void Interact(GameObject player)
        {
            player.GetComponent<CharacterController>().enabled = false;

            int chosenIndex = 0;
            float bestDistance = float.MaxValue;

            //We place the player to look on the side he is the closest to
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                float distance = Vector3.Distance(player.transform.position, gameObject.transform.GetChild(i).transform.position);
                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    chosenIndex = i;
                }
            }

            player.transform.position = gameObject.transform.GetChild(chosenIndex).transform.position + new Vector3(0, -2.6f, 0);
            player.transform.rotation = gameObject.transform.GetChild(chosenIndex).transform.rotation;

            //We can mess with the camera here, otherwise, the player sees under the map for a brief moment
            //This temporary fix doesn't look very good though, so for now it will be a necessary evil and will be fixed later. If we absolutely despise it, uncomment this
            //Camera cam = player[0].GetComponent<PlayerMovement>().camera;
            //cam.transform.position = gameObject.transform.GetChild(chosenIndex).transform.position;

            player.GetComponent<CharacterController>().enabled = true;
        }
    }
}

﻿using System;
using System.Collections;
using Supervisors;
using UnityEngine;

namespace Player.Scripts
{
    public class PlayerMovement : MonoBehaviour
    {
        public enum State
        {
            Idle = 0,
            Hidden = 1,
            Crouching = 2,
            Walking = 3,
            Running = 4
        }

        public event Action<State> OnStateChange;
        public event Action<Vector3> OnPositionChange;

        public CharacterController controller;
        public Camera camera;
        public float defaultSpeed = 6f;
        public float runSpeed = 30f;
        public float crawlSpeed = 3f;
        public float selectionDistance = 2f;
        public float maxStamina = 100f;
        public float staminaDepletionRate = 0.1f;
        public float standingHeight = 1.8f;
        public float crouchHeight = 1.2f;
        public float standingRadius = 0.35f;
        public float crouchRadius = 0.6f;
        public float proneHeight = 0.2f;
        public float proneRadius = 0.2f;
        
        private Animator animator;
        private Transform currentSelection;
        private Vector3 oldPosition;
        private float currentVerticalSpeed = 0;
        private float currentSpeed;
        private float targetHeight;
        private float targetRadius;
        private bool isHidden = false;

        private const float GRAVITY = -9.81f;
        private const float INTERACTION_TIMING = 0.1f;
        private const float CAMERA_ADVANCEMENT = 0.1f;

        //Animator constants
        private static readonly int HORIZONTAL = Animator.StringToHash("HorizontalAxis");
        private static readonly int VERTICAL = Animator.StringToHash("VerticalAxis");
        private static readonly int IS_WALKING = Animator.StringToHash("IsWalking");
        private static readonly int IS_RUNNING = Animator.StringToHash("IsRunning");
        private static readonly int IS_CROUCHING = Animator.StringToHash("IsCrouching");
        private static readonly int PICK_UP_TRIGGER = Animator.StringToHash("PickUp");
        private static readonly int HIDDEN_TRIGGER = Animator.StringToHash("Hidden");

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            InvokeRepeating(nameof(NotifyPositionChange), 0f, 0.1f);
        }

        // Update is called once per frame
        void Update()
        {
            //Drastic solution to prevent the stamina bar draining in the pause menu
            if (Time.timeScale == 0)
            {
                return;
            }

            Move();
            Interact(); //Putting interact here makes the interaction work everytime, but may be costly in the long term. It's a point we should discuss later
        }

        void FixedUpdate()
        {
            //Interact() was previously here. It is still debatable if the Interact function should be here or in Update, so this comment stays here in the meantime
        }
        
        private void NotifyPositionChange() =>
            OnPositionChange?.Invoke(transform.position);

        public void Move()
        {
            float horizontal = Input.GetAxis(InputManager.HORIZONTAL_INPUT);
            float vertical = Input.GetAxis(InputManager.VERTICAL_INPUT);

            animator.SetFloat(HORIZONTAL, horizontal);
            animator.SetFloat(VERTICAL, vertical);

            Vector3 move = transform.right * horizontal + transform.forward * vertical;
            
            if (isHidden)
            {
                PlayerHide();
                animator.SetTrigger(HIDDEN_TRIGGER);

                move = Vector3.zero;    //The player can't move while he is hidden
                ImmediateChangeControllerSize(standingHeight - 0.2f);

                //If we click again anywhere, the player gets out of the hiding place
                if (Input.GetButtonDown(InputManager.INTERACT_INPUT))
                {
                    PlayerIdle();
                    controller.enabled = false;
                    transform.position = oldPosition;
                    controller.enabled = true;

                    //We stand back up, so we change the controller size back to the idle size. We could also use the ChangeControllerSize function for this
                    ImmediateChangeControllerSize(standingHeight / 2);

                    isHidden = false;
                }

            }       //Will probably have to be optimized, fine for a first draft version
            else if (horizontal != 0 || vertical != 0 )
            {
                if (Input.GetButton(InputManager.RUN_INPUT) && StaminaBar.instance.GetCurrentStamina() > 0 && vertical > 0)
                {
                    PlayerRun();
                }
                else if (Input.GetButton(InputManager.CROUCH_INPUT))
                {
                    PlayerCrouch();
                }
                else
                {
                    PlayerWalk();
                }
                ChangeControllerSize();
            }
            else
            {
                if (Input.GetButton(InputManager.CROUCH_INPUT))
                {
                    PlayerCrouch();
                }
                else
                {
                    PlayerIdle();
                }
                ChangeControllerSize();
            }

            if (controller.isGrounded)
            {
                currentVerticalSpeed = 0;
            }

            currentVerticalSpeed += GRAVITY * Time.deltaTime;
            move.y = currentVerticalSpeed;

            controller.Move(move * currentSpeed * Time.deltaTime);
        }

        public void Interact()
        {
            //Since the main camera should theoretically be the only camera and it's the one used for the player's view, we use it as the ray's starting point
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * selectionDistance, Color.red);

            //We use the created ray to check for collisions with objects tagged as selectable. If we collide with one, it becomes our current selection
            if (Physics.Raycast(ray, out RaycastHit hitInfo, selectionDistance))
			{
                if (hitInfo.transform.CompareTag(TagManager.SELECTABLE) || hitInfo.transform.CompareTag(TagManager.HIDING_PLACE))
                {
                    currentSelection = hitInfo.transform;

                    InteractPrompt.instance.gameObject.SetActive(true);
                    InteractPrompt.instance.SetObjectName(currentSelection.name);

                    if (Input.GetButtonDown(InputManager.INTERACT_INPUT))
                    {
                        StartCoroutine(Interaction(currentSelection.gameObject));
                    }
                }
            }
            else
            {
                currentSelection = null;
                InteractPrompt.instance.gameObject.SetActive(false);
                InteractPrompt.instance.SetObjectName("");
            }
        }

        public void ChangeControllerSize()
        {
            controller.height = Mathf.Lerp(controller.height, targetHeight, 5f * Time.deltaTime);
            controller.radius = Mathf.Lerp(controller.radius, targetRadius, 5f * Time.deltaTime);
            controller.center = new Vector3(0, Mathf.Lerp(controller.height / 2, targetHeight / 2, 5f * Time.deltaTime), 0);

            //We always put the camera near the eyes of the model (Child 2 is the Eyelashes)
            //This gives us a nice little bobbing when walking and running, as well as when the player is idle
            camera.transform.position = Vector3.Lerp(camera.transform.position,
                transform.GetChild(2).GetComponent<SkinnedMeshRenderer>().bounds.center + transform.forward * CAMERA_ADVANCEMENT, 20f * Time.deltaTime);
        }

        //Used when hiding because Lerp is too slow and doesn't work
        public void ImmediateChangeControllerSize(float centerPosition)
        {
            controller.height = targetHeight;
            controller.radius = targetRadius;
            controller.center = new Vector3(0, centerPosition, 0);   //We put the controller around the head

            camera.transform.position = Vector3.Lerp(camera.transform.position,
                transform.GetChild(2).GetComponent<SkinnedMeshRenderer>().bounds.center + transform.forward * CAMERA_ADVANCEMENT, 20f * Time.deltaTime);
        }

        private IEnumerator Interaction(GameObject interactable)
        {
            string tag = interactable.tag;

            switch (tag)
            {
                case TagManager.SELECTABLE:
                    animator.SetTrigger(PICK_UP_TRIGGER);
                    yield return new WaitForSeconds(INTERACTION_TIMING);
                    interactable.GetComponent<IInteractable>().Interact(transform.gameObject);
                    break;

                case TagManager.HIDING_PLACE:
                    isHidden = true;
                    oldPosition = transform.position;   //We keep the position of the player before it enters the hiding place in order to place him back here once he exits
                    interactable.GetComponent<IInteractable>().Interact(transform.gameObject);
                    break;
            }
        }

        private void PlayerCrouch()
        {
            SetPlayerControllerSettings(false, false, true, crouchHeight, crouchRadius, crawlSpeed);
            OnStateChange?.Invoke(State.Crouching);
        }

        private void PlayerWalk()
        {
            SetPlayerControllerSettings(true, false, false, standingHeight, standingRadius, defaultSpeed);
            OnStateChange?.Invoke(State.Walking);
        }

        private void PlayerRun()
        {
            SetPlayerControllerSettings(false, true, false, standingHeight, standingRadius, runSpeed);
            StaminaBar.instance.UseStamina(staminaDepletionRate);
            OnStateChange?.Invoke(State.Running);
        }

        private void PlayerHide()
        {
            SetPlayerControllerSettings(false, false, false, proneHeight, proneRadius, defaultSpeed);
        }

        private void PlayerIdle()
        {
            SetPlayerControllerSettings(false, false, false, standingHeight, standingRadius, defaultSpeed);
            OnStateChange?.Invoke(State.Idle);
        }

        private void SetPlayerControllerSettings(bool walkStatus, bool runStatus, bool crouchStatus, float height,
            float radius, float speed)
        {
            animator.SetBool(IS_WALKING, walkStatus);
            animator.SetBool(IS_RUNNING, runStatus);
            animator.SetBool(IS_CROUCHING, crouchStatus);

            targetHeight = height;
            targetRadius = radius;
            currentSpeed = speed;
        }

        public bool GetHiddenStatus()
        {
            return isHidden;
        }
    }
}
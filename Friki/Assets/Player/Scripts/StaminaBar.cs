﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public Slider staminaBar;

    private float maxStamina = 100f;
    private float currentStamina;
    public float regenDelay = 1f;
    public float regenRate = 0.05f;

    public static StaminaBar instance;

    private Coroutine regeneration;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentStamina = maxStamina;
        staminaBar.maxValue = maxStamina;
        staminaBar.value = maxStamina;
    }

    public void UseStamina(float amount)
    {
        if (currentStamina > 0)
        {
            currentStamina -= amount;
            staminaBar.value = currentStamina;

            //If this coroutine is started again while regen is active, we stop the regeneration process
            if (regeneration != null){
                StopCoroutine(regeneration);
            }

            regeneration = StartCoroutine(RegenStamina());
        }
    }

    private IEnumerator RegenStamina()
    {
        //Little delay before starting to regenerate stamina
        yield return new WaitForSeconds(regenDelay);

        //We regen as long as the stamina bar isn't full or coroutine isn't stopped
        while (currentStamina < maxStamina)
        {
            currentStamina += maxStamina / 100;
            staminaBar.value = currentStamina;
            yield return new WaitForSeconds(regenRate);
        }

        //Regeneration of stamina is complete
        regeneration = null;
    }

    public float GetCurrentStamina()
    {
        return currentStamina;
    }

}

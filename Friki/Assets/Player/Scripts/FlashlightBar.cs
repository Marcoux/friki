﻿using UnityEngine;
using UnityEngine.UI;

namespace Player.Scripts
{
    
    public class FlashlightBar : MonoBehaviour
    {
        public Slider flashlightBar;
        public static FlashlightBar instance;

        private const float MAX_BATTERY_LEVEL = 100f;
        private float currentBatteryLevel;

        private void Awake()
        {
            instance = this;
        }

        // Start is called before the first frame update
        private void Start()
        {
            currentBatteryLevel = MAX_BATTERY_LEVEL;
            flashlightBar.maxValue = MAX_BATTERY_LEVEL;
            flashlightBar.value = MAX_BATTERY_LEVEL;
        }

        public void UseBattery(float amount)
        {
            if (currentBatteryLevel > 0)
            {
                currentBatteryLevel -= amount / 50;
                flashlightBar.value = currentBatteryLevel;
            }
        }

        public float GetCurrentBatteryLevel()
        {
            return currentBatteryLevel;
        }
    }
}

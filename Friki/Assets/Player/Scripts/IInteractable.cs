﻿using UnityEngine;

namespace Player.Scripts
{
    public interface IInteractable
    {
        void Interact(GameObject player);
    }
}


﻿using Menus.Scripts;
using UnityEngine;

namespace Player.Scripts
{
    public class CentralDotUpdate : MonoBehaviour
    {
        private void Awake()
        {
            gameObject.SetActive(PlayerOptionsManager.LoadOrDefault().isShowCentralDot);
        }
    }
}

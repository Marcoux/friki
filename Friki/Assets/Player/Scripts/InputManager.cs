﻿namespace Player.Scripts
{
    public static class InputManager
    {
        public const string HORIZONTAL_INPUT = "Horizontal";
        public const string VERTICAL_INPUT = "Vertical";
        public const string HORIZONTAL_AXIS = "Mouse X";
        public const string VERTICAL_AXIS = "Mouse Y";

        public const string RUN_INPUT = "Run";
        public const string CROUCH_INPUT = "Crouch";
        public const string INTERACT_INPUT = "Interact";
        public const string FLASHLIGHT_INPUT = "Flashlight";
    }
}
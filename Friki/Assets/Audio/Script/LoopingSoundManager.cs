using Supervisors;
using UnityEngine;
using UnityEngine.Audio;

namespace Audio.Script
{
    internal class LoopingSoundManager : MonoBehaviour
    {
        [SerializeField] private AudioMixerSnapshot normal;
        [SerializeField] private AudioMixerSnapshot pause;
        
        [SerializeField] private AudioMixerSnapshot safe;
        [SerializeField] private AudioMixerSnapshot danger;
        
        private AudioSource levelSafeMusic;
        private AudioSource levelDangerMusic;
        private AudioSource levelWindSound;
        private AudioSource menuMusic;

        public static LoopingSoundManager Instance;

        private void Awake()
        {
            if (Instance is null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);

                /*
                 * . transform (music manager)
                 * ├───0 (Level safe game object)
                 * ├───1 (Level danger game object)
                 * ├───2 (Wind sound game object)
                 * ├───3 (Menu game object)
                 */
                levelSafeMusic = transform.GetChild(0).GetComponent<AudioSource>();
                levelDangerMusic = transform.GetChild(1).GetComponent<AudioSource>();
                levelWindSound = transform.GetChild(2).GetComponent<AudioSource>();
                menuMusic = transform.GetChild(3).GetComponent<AudioSource>();

                SceneLoader.AddSounds(new[] {menuMusic}, new[] {levelSafeMusic, levelDangerMusic, levelWindSound});
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void TransitionToNormal() => normal.TransitionTo(0.1f);

        public void TransitionToPause() => pause.TransitionTo(0.1f);
        
        public void TransitionToSafe() => safe.TransitionTo(0.2f);

        public void TransitionToDanger() => danger.TransitionTo(0.2f);
    }
}
using System;
using Monsters.Scripts;
using Monsters.Scripts.Attributes;
using UnityEngine;
using Random = System.Random;

namespace Audio.Script
{
    public class MonsterSoundMaker : MonoBehaviour
    {
        private static readonly Random rng = new Random();
        
        [SerializeField] private AudioClip[] yellingClips;
        [SerializeField] private AudioClip[] walkingClips;

        private AudioSource yellingSource;
        private AudioSource walkingSource;

        private void Start()
        { 
            yellingSource = transform.GetChild(0).GetComponent<AudioSource>();
            walkingSource = transform.GetChild(1).GetComponent<AudioSource>();

            var movement = GetComponentInParent<Monster>().movement;
            movement.OnActionChange += state =>
            {
                switch (state)
                {
                    case Movement.ActionValue.Idle:
                        break;
                    case Movement.ActionValue.WalkAndRunning:
                        if (walkingSource.isPlaying is false)
                        {
                            PlayRandomWalkingClip();
                        }
                        break;
                    case Movement.ActionValue.Attack1:
                    case Movement.ActionValue.Attack2:
                    case Movement.ActionValue.Attack3:
                        if (yellingSource.isPlaying is false)
                        {
                            PlayRandomYellingClip();
                        }
                        break;
                    case Movement.ActionValue.OpenDoor:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(state), state, null);
                }
            };
            movement.OnBattleChange += state =>
            {
                switch (state)
                {
                    case Movement.BattleValue.NotInBattle:
                        LoopingSoundManager.Instance.TransitionToSafe();
                        break;
                    case Movement.BattleValue.InBattle:
                        LoopingSoundManager.Instance.TransitionToDanger();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(state), state, null);
                }
            };
        }
        
        private void PlayRandomYellingClip()
        {
            var index = rng.Next(0, yellingClips.Length);
            yellingSource.PlayOneShot(yellingClips[index]);
        }
        
        private void PlayRandomWalkingClip()
        {
            var index = rng.Next(0, walkingClips.Length);
            walkingSource.PlayOneShot(walkingClips[index]);
        }
    }
}
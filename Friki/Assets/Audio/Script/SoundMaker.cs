using System;
using UnityEngine;
using Random = System.Random;

namespace Audio.Script
{
    public class SoundMaker
    {
        private static readonly Random rng = new Random();

        private readonly AudioClip[] clips;
        private readonly AudioSource source;
        private readonly Action<AudioSource> fadeIn;
        private readonly Action<AudioSource> fadeOut;

        public SoundMaker(
            AudioClip[] clips,
            AudioSource source,
            Action<AudioSource> fadeIn,
            Action<AudioSource> fadeOut)
        {
            this.clips = clips;
            this.source = source;
            this.fadeIn = fadeIn;
            this.fadeOut = fadeOut;
        }

        public void FadeIn()
        {
            if (source.isPlaying is false)
            {
                PlayRandomClip();
            }
            else
            {
                fadeIn(source);
            }
        }

        public void FadeOut()
        {
            if (source.isPlaying is true)
            {
                fadeOut(source);
            }
        }

        private void PlayRandomClip()
        {
            var index = rng.Next(0, clips.Length);
            source.PlayOneShot(clips[index]);
        }
    }
}
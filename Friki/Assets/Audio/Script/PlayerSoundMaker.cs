using System;
using Player.Scripts;
using UnityEngine;

namespace Audio.Script
{
    public class PlayerSoundMaker : MonoBehaviour
    {
        [SerializeField] private AudioClip[] breathingClips;
        [SerializeField] private AudioClip[] clothingClips;
        [SerializeField] private AudioClip[] walkingClips;

        private SoundMaker breathing;
        private SoundMaker clothing;
        private SoundMaker walking;

        private void Start()
        {
            var breathingSource = transform.GetChild(0).GetComponent<AudioSource>();
            var breathingFadeIn = FadeIn(0.4f, breathingSource.volume, 0.3f);
            var breathingFadeOut = FadeOut(0.2f, breathingSource.volume, 2.5f);
            breathing = new SoundMaker(breathingClips, breathingSource, breathingFadeIn, breathingFadeOut);

            var clothingSource = transform.GetChild(1).GetComponent<AudioSource>();
            var clothingFadeIn = FadeIn(0.3f, breathingSource.volume, 0.5f);
            var clothingFadeOut = FadeOut(0.0f, breathingSource.volume, 0.1f);
            clothing = new SoundMaker(clothingClips, clothingSource, clothingFadeIn, clothingFadeOut);
            
            var walkingSource = transform.GetChild(2).GetComponent<AudioSource>();
            var walkingFadeIn = FadeIn(1.0f, breathingSource.volume, 0.1f);
            var walkingFadeOut = FadeOut(0.0f, breathingSource.volume, 0.1f);
            walking = new SoundMaker(walkingClips, walkingSource, walkingFadeIn, walkingFadeOut);

            GetComponentInParent<PlayerMovement>().OnStateChange += state =>
            {
                switch (state)
                {
                    case PlayerMovement.State.Idle:
                    case PlayerMovement.State.Hidden:
                    case PlayerMovement.State.Crouching:
                        breathing.FadeOut();
                        clothing.FadeOut();
                        walking.FadeOut();
                        break;

                    case PlayerMovement.State.Running:
                        breathing.FadeIn();
                        clothing.FadeIn();
                        walking.FadeIn();
                        break;

                    case PlayerMovement.State.Walking:
                        breathing.FadeOut();
                        clothing.FadeOut();
                        walking.FadeIn();
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(state), state, null);
                }
            };
        }

        private static Action<AudioSource> FadeIn(float min, float max, float duration) =>
            source =>
            {
                if (source.volume < min)
                {
                    source.volume = min;
                }
                else if (source.volume < max)
                {
                    source.volume += Time.deltaTime / duration;
                }
            };

        private static Action<AudioSource> FadeOut(float min, float max, float duration) =>
            source =>
            {
                if (source.volume < min)
                {
                    source.Stop();
                    source.volume = max;
                }
                else
                {
                    source.volume -= Time.deltaTime / duration;
                }
            };
    }
}
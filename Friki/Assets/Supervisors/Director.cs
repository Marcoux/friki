using System.Collections.Generic;
using System.Linq;
using Player.Scripts;
using UnityEngine;
using Random = System.Random;

namespace Supervisors
{
    public class Director : MonoBehaviour
    {
        private static readonly Random rng = new Random();

        private static Vector3[] corridors;
        private static Vector3[] rooms;

        public static PlayerMovement.State PlayerMovement { get; private set; }
        public static Vector3 PlayerPosition { get; private set; }

        private void Awake()
        {
            // player init related
            var player = GameObject
                .FindWithTag(TagManager.PLAYER)
                .GetComponent<PlayerMovement>();

            player.OnStateChange += state => PlayerMovement = state;
            player.OnPositionChange += position => PlayerPosition = position;

            // director's waypoint init related
            /*
             * . transform (director)
             * ├───0 (all room waypoint) 
             * ├───1 (all corridor waypoint)
             */
            rooms = transform
                .GetChild(0)
                .Cast<Transform>()
                .Select(room => room.position)
                .ToArray();

            corridors = transform
                .GetChild(1)
                .Cast<Transform>()
                .Select(corridor => corridor.position)
                .ToArray();
        }

        public static IEnumerable<Vector3> PlayerNearestWaypoints(int size) =>
            rooms
                .Concat(corridors)
                .OrderBy(waypoint => Vector3.Distance(waypoint, PlayerPosition))
                .Take(size);

        public static Vector3 PlayerNearestWaypoint() =>
            rooms
                .Concat(corridors)
                .OrderBy(waypoint => Vector3.Distance(waypoint, PlayerPosition))
                .First();

        public static IEnumerable<Vector3> RandomRoomsWaypoint(int size) =>
            rooms.OrderBy(waypoint => rng.Next()).Take(size);
    }
}
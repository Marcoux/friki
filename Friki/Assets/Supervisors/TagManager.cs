namespace Supervisors
{
    public static class TagManager
    {
        public const string MONSTER = "Monster";
        public const string PLAYER = "Player";
        public const string SELECTABLE = "Selectable";
        public const string HIDING_PLACE = "Hiding Place";

        public const string DOOR_CLOSE = "Selectable";
        public const string DOOR_OPENING = "DoorOpening";
        public const string DOOR_OPEN = "OpenDoor";
    }
}
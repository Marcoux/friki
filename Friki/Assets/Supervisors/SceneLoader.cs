using UnityEngine;
using UnityEngine.SceneManagement;

namespace Supervisors
{
    public static class SceneLoader
    {
        private const int MAIN_MENU = 0;
        private const int MAIN_LEVEL = 1;
        private const int GAME_OVER_MENU = 2;
        private const int SCENE_VICTORY = 3;

        public static void AddSounds(AudioSource[] menuSounds, AudioSource[] levelSounds)
        {
            SceneManager.sceneLoaded += (scene, _) =>
            {
                switch (scene.buildIndex)
                {
                    case MAIN_MENU:
                        foreach (var sound in menuSounds)
                        {
                            sound.Play();
                        }
                        break;
                    case MAIN_LEVEL:
                        foreach (var sound in levelSounds)
                        {
                            sound.Play();
                        }
                        break;
                }
            };
            
            SceneManager.sceneUnloaded += scene =>
            {
                switch (scene.buildIndex)
                {
                    case MAIN_MENU:
                        foreach (var sound in menuSounds)
                        {
                            sound.Stop();
                        }
                        break;
                    case MAIN_LEVEL:
                        foreach (var sound in levelSounds)
                        {
                            sound.Stop();
                        }
                        break;
                }
            };
        }

        public static void LoadMainMenu() =>
            SceneManager.LoadScene(MAIN_MENU);

        public static void LoadMainLevel() =>
            SceneManager.LoadScene(MAIN_LEVEL);

        public static void LoadGameOver() =>
            SceneManager.LoadScene(GAME_OVER_MENU);

        public static void LoadVictory() =>
            SceneManager.LoadScene(SCENE_VICTORY);
    }
}
﻿using Supervisors;
using UnityEngine;

namespace Menus.Scripts.Start
{
    public class StartMenu : MonoBehaviour
    {
        private void Awake()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        
        public void PlayGame()
        {
            SceneLoader.LoadMainLevel();
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
using System;
using UnityEngine;

namespace Menus.Scripts
{
  /// <summary>
  ///     This class represent all possible options that player may tweak.
  /// </summary>
  /// <remarks>
  ///     TODO: this could be used by others components then the menu! Should be place in another namespace
  /// </remarks>
  [CreateAssetMenu(menuName = "ScriptableObjects/menuOptions")]
    [Serializable]
    public class GameOptions : ScriptableObject
    {
        private const float DEFAULT_VOLUME = 1.0f;

        // These are system independent values
        public bool isFullscreen;
        public bool isShowCentralDot;
        public float masterVolume;
        public float musicVolume;
        public float soundVolume;

        // These are system dependant values
        [HideInInspector] public int height;
        [HideInInspector] public int refreshRate;
        [HideInInspector] public int width;

        public static GameOptions Default()
        {
            var defaultOptions = Resources.Load<GameOptions>("defaultOptions");

            defaultOptions.masterVolume = DEFAULT_VOLUME;
            defaultOptions.musicVolume = DEFAULT_VOLUME;
            defaultOptions.soundVolume = DEFAULT_VOLUME;

            defaultOptions.isShowCentralDot = true;

            // These are system dependant default values
            var resolution = Screen.currentResolution;

            defaultOptions.height = resolution.height;
            defaultOptions.refreshRate = resolution.refreshRate;
            defaultOptions.width = resolution.width;

            return Instantiate(defaultOptions); // this create a clone
        }
    }
}
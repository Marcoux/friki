﻿using Audio.Script;
using Supervisors;
using UnityEngine;

namespace Menus.Scripts.Pause
{
    public class PauseMenu : MonoBehaviour
    {
        private const string ESCAPE_INPUT = "Esc";
        private const string OPTIONS_MENU_NAME = "OptionsMenu";

        private struct TimeScaleValue
        {
            public const int Stop = 0, Normal = 1;
        }

        private Canvas pauseCanvas;
        private GameObject optionsMenuObject;

        public void Resume()
        {
            Disable();
        }

        public void LoadOptions()
        {
            optionsMenuObject.SetActive(true);
        }

        public void Return()
        {
            Disable();
            SceneLoader.LoadMainMenu();
        }

        public void Quit()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        private void Start()
        {
            pauseCanvas = GetComponent<Canvas>();
            optionsMenuObject = gameObject.transform.Find(OPTIONS_MENU_NAME).gameObject;
        }

        private void Update()
        {
            if (optionsMenuObject.activeSelf is false && Input.GetButtonDown(ESCAPE_INPUT))
            {
                if (pauseCanvas.enabled is true)
                {
                    Disable();
                }
                else
                {
                    Enable();
                }
            }
        }
        
        private void Disable()
        {
            Time.timeScale = TimeScaleValue.Normal;
            
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            
            pauseCanvas.enabled = false;
            
            LoopingSoundManager.Instance.TransitionToNormal();
        }

        private void Enable()
        {
            Time.timeScale = TimeScaleValue.Stop;
            
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            
            pauseCanvas.enabled = true;

            LoopingSoundManager.Instance.TransitionToPause();
        }
    }
}

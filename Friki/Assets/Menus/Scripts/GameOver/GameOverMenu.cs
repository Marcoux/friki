﻿using Supervisors;
using UnityEngine;

namespace Menus.Scripts.GameOver
{
    public class GameOverMenu : MonoBehaviour
    {
        private void Awake()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public void RetryGame()
        {
            SceneLoader.LoadMainLevel();
        }

        public void BackToMainMenu()
        {
            SceneLoader.LoadMainMenu();
        }
    }
}
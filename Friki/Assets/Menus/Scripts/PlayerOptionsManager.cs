﻿using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace Menus.Scripts
{
  /// <summary>
  ///     This class is kinda like a singleton (but with static) and will read the player's options from disk.
  ///     Same for write.
  /// </summary>
  /// <remarks>
  ///     The class name could be better...
  /// </remarks>
  public static class PlayerOptionsManager
    {
        private static readonly string filepath = Application.persistentDataPath + "/playerOptions.json";

        public static GameOptions LoadOrDefault()
        {
            return File.Exists(filepath) ? Load() : GameOptions.Default();
        }

        public static GameOptions Load()
        {
            string json;
            using (var reader = File.OpenText(filepath))
            {
                json = reader.ReadToEnd();
            }

            var options = ScriptableObject.CreateInstance<GameOptions>();
            JsonUtility.FromJsonOverwrite(json, options);
            return options;
        }

        public static void Save(GameOptions options)
        {
            var json = JsonUtility.ToJson(options);
            using (var writer = File.CreateText(filepath))
            {
                writer.Write(json);
            }
        }

        public static async Task SaveAsync(GameOptions options)
        {
            var json = JsonUtility.ToJson(options);
            using (var writer = File.CreateText(filepath))
            {
                await writer.WriteAsync(json).ConfigureAwait(false);
            }
        }
    }
}
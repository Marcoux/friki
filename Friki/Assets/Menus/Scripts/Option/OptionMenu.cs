﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Menus.Scripts.Option
{
    public class OptionMenu : MonoBehaviour
    {
        private const int GRAPHIC_PANEL = 0;
        private const int SOUND_PANEL = 1;

        private const string VOLUME = "Volume";

        //Graphic components
        [SerializeField] private TMP_Dropdown modeDropdown;
        [SerializeField] private TMP_Dropdown resolutionDropdown;
        [SerializeField] private Toggle toggleCentralDot;
        [SerializeField] private GameObject centralDotObject;

        //Sound components
        [SerializeField] private Slider sliderMaster;
        [SerializeField] private Slider sliderMusic;
        [SerializeField] private Slider sliderSound;
        [SerializeField] private AudioMixer mixerMaster;
        [SerializeField] private AudioMixer mixerMusic;
        [SerializeField] private AudioMixer mixerSound;

        //Components for tab options
        [SerializeField] private GameObject graphicOptionsPanel;
        [SerializeField] private GameObject soundOptionsPanel;

        private GameOptions currentOptions;
        private Resolution[] resolutions;

        /// <summary>
        ///     This method will initialize the menu but will ALSO setup the game according to player options.
        ///     If it's the first time the game is run, then the player's options will be set to default.
        /// </summary>
        private void Awake()
        {
            currentOptions = PlayerOptionsManager.LoadOrDefault();
            resolutions = Screen.resolutions;

            resolutionDropdown.ClearOptions();
            resolutionDropdown.AddOptions(
                resolutions.Select(resolution => resolution.ToString()).ToList()
            );

            Apply();
        }

        private void OnEnable()
        {
            currentOptions = PlayerOptionsManager.Load();
            ChangeTab(GRAPHIC_PANEL);
            RefreshSliders();
            RefreshToggle();
            RefreshDropdowns();
        }

        private void RefreshToggle()
        {
            toggleCentralDot.isOn = currentOptions.isShowCentralDot;
        }

        private void RefreshDropdowns()
        {
            modeDropdown.value = currentOptions.isFullscreen is true ? 0 : 1;
            modeDropdown.RefreshShownValue();

            var resolution = new Resolution
            {
                height = currentOptions.height,
                width = currentOptions.width,
                refreshRate = currentOptions.refreshRate
            };
            resolutionDropdown.value = Array.IndexOf(resolutions, resolution);
            resolutionDropdown.RefreshShownValue();
        }

        private void RefreshSliders()
        {
            sliderMaster.value = currentOptions.masterVolume;
            sliderMusic.value = currentOptions.musicVolume;
            sliderSound.value = currentOptions.soundVolume;
        }

        public void OnModeChange(int mode)
        {
            currentOptions.isFullscreen = mode is 0;
        }

        public void ActivateGraphicPanel()
        {
            ChangeTab(GRAPHIC_PANEL);
        }

        public void ActivateSoundPanel()
        {
            ChangeTab(SOUND_PANEL);
        }

        private void ChangeTab(int tabIndex)
        {
            switch(tabIndex)
            {
                case GRAPHIC_PANEL:
                    graphicOptionsPanel.SetActive(true);
                    soundOptionsPanel.SetActive(false);
                    break;
                case SOUND_PANEL:
                    graphicOptionsPanel.SetActive(false);
                    soundOptionsPanel.SetActive(true);
                    break;
            }
        }

        public void OnMasterValueChanged(float sliderValue)
        {
            if(currentOptions != null)
                currentOptions.masterVolume = sliderValue;
        }

        public void OnMusicValueChanged(float sliderValue)
        {
            if (currentOptions != null)
                currentOptions.musicVolume = sliderValue;
        }

        public void OnSoundValueChanged(float sliderValue)
        {
            if (currentOptions != null)
                currentOptions.soundVolume = sliderValue;
        }

        public void OnToggleCentralDotValueChanged(bool toggleValue)
        {
            if (currentOptions != null)
                currentOptions.isShowCentralDot = toggleValue;
        }

        public void OnResolutionChange(int index)
        {
            var resolution = resolutions[index];

            currentOptions.height = resolution.height;
            currentOptions.width = resolution.width;
            currentOptions.refreshRate = resolution.refreshRate;
        }

        public void Apply()
        {
            // TODO: consider save async if sync save is to long
            PlayerOptionsManager.Save(currentOptions);

            Screen.SetResolution(
                currentOptions.width,
                currentOptions.height,
                currentOptions.isFullscreen,
                currentOptions.refreshRate);

            mixerMaster.SetFloat(VOLUME, Mathf.Log10(currentOptions.masterVolume) * 20);
            mixerMusic.SetFloat(VOLUME, Mathf.Log10(currentOptions.musicVolume) * 20);
            mixerSound.SetFloat(VOLUME, Mathf.Log10(currentOptions.soundVolume) * 20);

            if (centralDotObject != null)
                centralDotObject.SetActive(currentOptions.isShowCentralDot);

            gameObject.SetActive(false);
        }

        public void Default()
        {
            currentOptions = GameOptions.Default();
            Apply();
        }
    }
}